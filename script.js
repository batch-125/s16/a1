
// IF PROMPT 

/*let firstName = prompt("Enter your First Name:")
let lastName = prompt("Enter your Last Name:")
let email = prompt("Enter your Email:")
let password1 = prompt("Enter your password:")
let password2 = prompt("Please re-enter your password")
getInfo(firstName, lastName, email, password1, password2);
*/



function getInfo(firstName, lastName, email, password1, password2){
		if(firstName.length>0 && lastName.length>0 && email.length>0 && password1 === password2 && password1.length >= 8){
			console.log("Thanks for logging your information");
		} else if (firstName.length == 0 || lastName.length == 0 || email.length == 0 ){
				if (password1.length <= 7 || password2.length <= 7){
					console.log("Please fill in your information.\nPassword should be at least 8 characters")
				} else if(password1 != password2 ){
					console.log("Please fill in your information.\nPassword did not match.")
				}else {
					console.log("Please fill in your information.");
				}
		
		} else if (password1.length <= 7 || password2.length <= 7){
				if (password1 != password2){
					console.log("Password should be at least 8 characters.\nPassword did not match.");
				} else {
					console.log("Password should be at least 8 characters.");
				}

		} else {
			console.log("Password did not match.");
		}
	}

// IF CONSOLE
getInfo("Aidan", "Bigornia", "aidanmbigornia@gmail.com", "12345678", "12345678");	  //correct
getInfo("Aidan", "Bigornia", "aidanmbigornia@gmail.com", "12345678", "123456789");	  //pw not match
getInfo("Aidan", "Bigornia", "aidanmbigornia@gmail.com", "1234567", "1234567");	      //kulang password
getInfo("", "Bigornia", "aidanmbigornia@gmail.com", "12345678", "12345678");   		  //fill in
getInfo("", "Bigornia", "aidanmbigornia@gmail.com", "12345678", "12345677");   		  //fill in and not match
getInfo("aidan", "Bigornia", "aidanmbigornia@gmail.com", "123478", "125678");   	  //not match and atleast 8

